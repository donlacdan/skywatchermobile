package com.flight.Skywatcher.service;

import com.flight.Skywatcher.model.FlightDetails;
import com.flight.Skywatcher.model.dto.FlightPassDetails;
import com.flight.Skywatcher.repository.FlightDetailRepository;
import com.flight.Skywatcher.service.impl.FlightWatchServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FlightWatchServiceTest {

    @InjectMocks
    FlightWatchServiceImpl flightWatchService;

    @Mock
    FlightDetailRepository flightDetailRepository;

    @Test
    void getFlightsNull() throws ParseException {
        List<FlightDetails> flightDetailsList = new ArrayList<>();
        flightDetailsList.add(new FlightDetails());
        when(flightDetailRepository.getByArrivalTime(any())).thenReturn(null);
        flightWatchService.getFlights("12-02-21", "UTC");
    }


    @Test
    void getFlightsNullValueInDate() throws ParseException {
        List<FlightDetails> flightDetailsList = new ArrayList<>();
        flightDetailsList.add(new FlightDetails());
        when(flightDetailRepository.getByArrivalTime(any())).thenReturn(flightDetailsList);
        flightWatchService.getFlights("12-02-21", "UTC");
    }

    @Test
    void getFlightsWithValueInDate() throws ParseException {
        List<FlightDetails> flightDetailsList = new ArrayList<>();
        flightDetailsList.add(new FlightDetails(1L, "null","null",new Date(),"null","null","null",new Date(),new Date()));
        when(flightDetailRepository.getByArrivalTime(any())).thenReturn(flightDetailsList);
        List<FlightPassDetails> flightDetails = flightWatchService.getFlights("12-02-21", "UTC").getDetailsList();
        Assertions.assertEquals(flightDetailsList.get(0).getDeparturePort(),flightDetails.get(0).getDeparturePort());
    }

    @Test
    void getFlightsByAirline() {

    }

    @Test
    void addFlights() {
    }

    @Test
    void saveFlightDto() {
    }
}