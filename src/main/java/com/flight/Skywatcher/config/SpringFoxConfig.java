package com.flight.Skywatcher.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;

@Configuration
@EnableSwagger2
public class SpringFoxConfig {
    @Value("${flight.service.version}")
    private String version;

    @Value("${flight.service.environment}")
    private String environment;

    /**
     * Creation of API Specification for Swagger
     * @return
     */
    @Bean
    public Docket apiDocket() {
        final ApiInfo info = new ApiInfo(
            "Flight Watching Microservice (" + environment + ")",
            "APIs for Flight Watching Queries",
            version,
            null,
            null,
            null,
            null,
            Arrays.asList()
        );
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.flight.Skywatcher"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(info);
    }
}
