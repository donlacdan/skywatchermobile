package com.flight.Skywatcher.service.impl;

import com.flight.Skywatcher.model.FlightDetails;
import com.flight.Skywatcher.model.dto.FlightPassDetails;
import com.flight.Skywatcher.model.dto.FlightResult;
import com.flight.Skywatcher.repository.*;
import com.flight.Skywatcher.service.FlightWatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Service
public class FlightWatchServiceImpl implements FlightWatchService {

    @Autowired
    FlightDetailRepository flightRepository;


    /**
     * Get Flights Details by date
     * @param date
     * @param timezone
     * @return FlightResult
     * @throws ParseException
     */
    @Override
    public FlightResult getFlights(final String date, final String timezone) throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone(timezone));
        // NOTE: Current API is made to be flexible for different Dates
        System.out.println(sdf.parse(DateTimeFormatter.ofPattern("MM-dd-yyyy", Locale.ENGLISH).format(LocalDateTime.now())));
        return new FlightResult(convertFlightPass(flightRepository.getByArrivalTime(sdf.parse(date))));
    }

    /**
     * Converts FlightDetail to FlightPassDetails
     * @param byArrivalTime
     * @return
     */
    private List<FlightPassDetails> convertFlightPass(List<FlightDetails> byArrivalTime) {
        List<FlightPassDetails> flightDetailsList = new ArrayList<>();
        if (Optional.ofNullable(byArrivalTime).isPresent()) {
            for (FlightDetails flightDetails : byArrivalTime) {
                FlightPassDetails passDetails = new FlightPassDetails(flightDetails);
                passDetails.setArrivalTime(parseDate(flightDetails.getArrivalTime()));
                passDetails.setDepartureTime(parseDate(flightDetails.getDepartureTime()));
                flightDetailsList.add(passDetails);
            }
        }
        return flightDetailsList;
    }

    /**
     * Parse Date(Timestamp) into UTC String
     * @param time
     * @return
     */
    private String parseDate(final Date time) {
        String parsedDate = "";
        if (time != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            parsedDate = dateFormat.format(time);
        }
        return parsedDate;

    }

    /**
     * Get Flights Details by Airline
     * @param airline
     * @return
     */
    @Override
    public FlightResult getFlightsByAirline(String airline) {
        return new FlightResult(convertFlightPass(flightRepository.findByAirline(airline)));

    }

    /**
     * Save Flights Details
     * @param flightdetails
     */
    @Override
    public void addFlights(FlightDetails flightdetails) {
        flightRepository.save(flightdetails);
        return;
    }

    /**
     * Save Flight Detail List
     * NOTE: for sample APIs ONLY
     * @param flightPassDetails
     */
    @Override
    public void saveFlightDto(List<FlightDetails> flightPassDetails) {
        flightRepository.saveAll(flightPassDetails);
        return;
    }

}
