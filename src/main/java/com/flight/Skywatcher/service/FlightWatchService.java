package com.flight.Skywatcher.service;

import com.flight.Skywatcher.model.FlightDetails;
import com.flight.Skywatcher.model.dto.FlightPassDetails;
import com.flight.Skywatcher.model.dto.FlightResult;

import java.text.ParseException;
import java.util.List;

public interface FlightWatchService {
    FlightResult getFlights(final String date, final String timezone) throws ParseException;
    FlightResult getFlightsByAirline(String airline);
    void addFlights(FlightDetails flightdetails);
    void saveFlightDto(List<FlightDetails> flightPassDetails);


    }
