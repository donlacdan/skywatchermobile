package com.flight.Skywatcher.repository;

import com.flight.Skywatcher.model.FlightDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface FlightDetailRepository extends JpaRepository<FlightDetails, Long> {

    List<FlightDetails> findByAirline(String airline);


    @Query(value = "SELECT * FROM Flight_Details u " +
            "WHERE FORMATDATETIME(u.arrival_Time, 'yyyy.MM.dd') = FORMATDATETIME(?1, 'yyyy.MM.dd')",
            nativeQuery = true)
    List<FlightDetails> getByArrivalTime(Date arrivalTime);

}
