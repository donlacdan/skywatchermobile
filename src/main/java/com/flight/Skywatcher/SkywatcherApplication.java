package com.flight.Skywatcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkywatcherApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkywatcherApplication.class, args);
	}

}
