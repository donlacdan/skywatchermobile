package com.flight.Skywatcher.controller;

import com.flight.Skywatcher.model.FlightDetails;
import com.flight.Skywatcher.model.dto.FlightResult;
import com.flight.Skywatcher.service.FlightWatchService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class FlightWatchController {
    @Autowired
    FlightWatchService flightWatchService;

    /**
     * Get Flights By Date
     * @param date
     * @return
     * @throws ParseException
     */
    @GetMapping("/flights")
    public ResponseEntity<FlightResult> getFlights(@RequestParam(value="date") String date) throws ParseException {
        return ResponseEntity.ok(flightWatchService.getFlights(date, "UTC"));
    }

    //TODO: REMOVE FOR SAMPLE ONLY
    @GetMapping("/addflights")
    public ResponseEntity<String> addFlights() {
        List<FlightDetails> flightPassDetailsList = new ArrayList<>();
        for (int i=0; i<10; i++){
            FlightDetails flightDetails = new FlightDetails();
            flightDetails.setFlightNumber("AKL12" + i);
            flightDetails.setDeparturePort("AKL");
            flightDetails.setArrivalTime(new Timestamp(System.currentTimeMillis() - 36000000 * i));
            flightDetails.setArrivalPort("MNL");
            flightDetails.setDepartureTime(new Timestamp(System.currentTimeMillis() - 36001010 * i));
            flightDetails.setAirline("AZ");
            flightPassDetailsList.add(flightDetails);
        }
        flightWatchService.saveFlightDto(flightPassDetailsList);
        return ResponseEntity.ok("Success");
    }

    /**
     * Get Flights By Airline Controller
     * @param airline
     * @return
     */
    @PostMapping("/flights/{airline}")
    public ResponseEntity<FlightResult> getFlightsByAirlines(@ApiParam(value="Sample Airline",
            example = "AKL", required = true) @PathVariable("airline") String airline) {
        return ResponseEntity.ok(flightWatchService.getFlightsByAirline(airline));
    }

    /**
     * Add Flights Controller
     * @param flightDetails
     * @return
     */
    @PostMapping("/flights")
    public ResponseEntity<String> addFlights(@ApiParam(value="Flight Details",
            required = true) @RequestBody FlightDetails flightDetails) {
        return ResponseEntity.ok("Success" );
    }

}
