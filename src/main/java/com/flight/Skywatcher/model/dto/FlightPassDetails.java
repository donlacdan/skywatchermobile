package com.flight.Skywatcher.model.dto;

import com.flight.Skywatcher.model.FlightDetails;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FlightPassDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long flightDetailsId;
    private String flightNumber;
    private String departurePort;
    private String arrivalPort;
    private String departureTime;
    private String arrivalTime;

    public FlightPassDetails(FlightDetails flightDetails) {
        this.flightDetailsId = flightDetails.getFlightDetailsId();
        this.flightNumber = flightDetails.getFlightNumber();
        this.departurePort = flightDetails.getDeparturePort();
        this.arrivalPort = flightDetails.getArrivalPort();
        this.departurePort = flightDetails.getDeparturePort();
    }
}
