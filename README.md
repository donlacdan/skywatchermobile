# Flight Watch Demo Program
This program is a demo for SkyWatchers, a flight watching mobile application.
## Reference Documentation
For further tech reference, please consider the sections:
### Tech Stack
1. H2
2. Java Spring Boot
3. Java Security
4. Java Junit
5. Swagger
### Build app
    gradlew build
    gradlew bootRun
### Database Link
    http://localhost:8080/h2-ui
### Swagger Link
    http://localhost:8080/swagger-ui.html

